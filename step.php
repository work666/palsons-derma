<!DOCTYPE html>
<html lang="<?= $lang; ?>">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Palsons Derma</title>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@100;200;300;400;500;600;700;800;900&display=swap"
      rel="stylesheet">
    <link rel="stylesheet"
      href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <header>
      <div class="hamburger"><i class="las la-bars"></i></div>
      <div class="userName">Hi Ravi</div>
    </header>
    <section id="main">
      <div class="userDetails">
        <div class="head">
          <h2>Fill up patient details<p class="text" style="display:none;">Harish, 65 yrs, Male</p>
          </h2>
          <div class="arrow"></div>
        </div>
        <form action="">
          <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" id="name">
          </div>
          <div class="mb-3">
            <label for="age" class="form-label">Age</label>
            <input type="number" class="form-control" id="age">
          </div>
          <div class="mb-3">
            <label for="age" class="form-label">Gender</label>
            <div class="checkbox">
              <input type="radio" id="male" name="radio-group">
              <label for="male">Male</label>
              <input type="radio" id="female" name="radio-group" checked>
              <label for="female">Female</label>
              <input type="radio" id="others" name="radio-group">
              <label for="others">Others</label>
            </div>
          </div>
          <div class="mt-3 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
      <div class="step">
        <span class="done"><i class="las la-check"></i></span>
        <span class="done"><i class="las la-check"></i></span>
        <span class="active">3</span>
        <span>4</span>
        <span>5</span>
      </div>
      <div class="question">
        <form action="">
          <div class="que">Does the scaling happen within your scapline or beyond?</div>
          <div class="custom_radio">
            <input type="radio" id="featured-1" name="featured" checked><label for="featured-1">Yes</label>
            <br>
            <input type="radio" id="featured-2" name="featured"><label for="featured-2">No</label>
          </div>
          <div class="mt-3 text-center">
            <button type="submit" class="btn buttonLight btn-primary">Back</button>
            <button type="submit" class="btn btn-primary">Next</button>
          </div>
        </form>
      </div>
      <div class="meterBar">
        <img src="img/meter-bar.png" alt="">
      </div>
    </section>
    <footer>
      <p>&copy; 2022 Palsons Derma | All Rights Reserved</p>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="script.js"></script>
  </body>
</html>