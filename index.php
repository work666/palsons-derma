<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Palsons Derma</title>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@100;200;300;400;500;600;700;800;900&display=swap"
      rel="stylesheet">
    <link rel="stylesheet"
      href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <header>
      <div class="hamburger"><i class="las la-bars"></i></div>
      <div class="userName">Hi Ravi</div>
    </header>
    <section id="main">
      <div class="login">
        <div class="logo"><img src="img/logo.png" alt=""></div>
        <div class="bg"><img src="img/login-bg.png" alt=""></div>
        <form action="">
          <h2>Login Details</h2>
          <div class="mb-3">
            <input type="text" class="form-control" id="username" placeholder="Username, email & phone number">
          </div>
          <div class="mb-3">
            <input type="password" class="form-control" id="password" placeholder="Password">
          </div>
          <div class="mb-3">
            <a class="forgot" href="#">Forgot Password ?</a>
          </div>
          <button type="submit" class="btn btn-primary">Login</button>
        </form>
      </div>
    </section>
    <footer>
      <p>&copy; 2022 Palsons Derma | All Rights Reserved</p>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="script.js"></script>
  </body>
</html>